#!/bin/bash
HN=$(hostname)
# echo $HN
if [ $HN == "raspberrypi3" ]
then
	git rm --cached nginx/www/index.html
	git rm --cached nginx/www/images/logo.png
        git rm --cached nginx/www/images/logo.png.old
fi
git add .
git diff --stat --cached origin/master
read -t 5 -r -s -p $'Wait 5 seconds or press enter to continue...'
echo ""
git commit -am "update from ${HOSTNAME}"
git push
